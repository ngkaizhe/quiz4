﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.ImageEffects;

public class ShaderController : MonoBehaviour
{
    public Dropdown dropdown;
    public MeshRenderer obj;
    public Material[] materials;
    public GameObject RGBSlider;
    public Slider[] sliders;

    private int value = 0;
    private bool hasChanged = false;

    private int blendImageDir = 1;

    public void OptionChange()
    {
        value = dropdown.value;
        hasChanged = true;
    }

    private void Update()
    {
        if (hasChanged)
        {
            reset();
            obj.material = materials[value];
        }
       
        // change shape
        if (value == 1)
        {

        }
        // change color
        if (value == 2)
        {
            RGBSlider.SetActive(true);
            float r = sliders[0].value;
            float g = sliders[1].value;
            float b = sliders[2].value;

            obj.material.SetColor("_MainColor", new Color(r/255, g/255, b/255));
        }
        // change negative color
        if (value == 3)
        {
            RGBSlider.SetActive(true);
            float r = sliders[0].value;
            float g = sliders[1].value;
            float b = sliders[2].value;

            obj.material.SetColor("_MainColor", new Color(1 - r / 255, 1 - g / 255, 1 - b / 255));
        }
        // grey
        if (value == 4)
        {
            
            
        }
        // blend Image
        if (value == 5)
        {
            float speed = 0.5f;
            float temp = obj.material.GetFloat("_Proportion");
            temp += Time.deltaTime * speed * blendImageDir;

            if (temp >= 1)
            {
                blendImageDir = -1;
                temp = 1;
            }
            else if (temp <= 0) {
                blendImageDir = 1;
                temp = 0;
            }

            obj.material.SetFloat("_Proportion", temp);
        }
    }

    private void reset()
    {
        hasChanged = false;
        Camera.main.GetComponent<MotionBlur>().blurAmount = 0;
        blendImageDir = 1;
        RGBSlider.SetActive(false);
    }

    public void OnBlur()
    {
        Camera.main.GetComponent<MotionBlur>().blurAmount = 0.92f;
    }

    public void OffBlur()
    {
        Camera.main.GetComponent<MotionBlur>().blurAmount = 0.0f;
    }
}
